package com.example.bottomsup.view.drink

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.bottomsup.adapter.DrinksAdapter
import com.example.bottomsup.databinding.FragmentDisplayBinding
import com.example.bottomsup.viewmodel.DrinkListViewModel

class DisplayFragment : Fragment() {

    private var _binding: FragmentDisplayBinding? = null
    private val binding get() = _binding!!
    private val drinkListViewModel by viewModels<DrinkListViewModel>() {
        DrinkListViewModelFactory(category = args.drink)
    }

    private val args by navArgs<DisplayFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDisplayBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        binding.rvDisplay.adapter = displayAdapter
        drinkListViewModel.state.observe((viewLifecycleOwner)) { state ->
            binding.loader.isVisible = state.isLoading
            if (state.drinks.isNotEmpty()) {
                binding.rvDisplay.adapter = DrinksAdapter(state.drinks) { drink ->
                    Toast.makeText(context, drink.strDrink, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}