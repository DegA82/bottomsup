package com.example.bottomsup.view.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bottomsup.viewmodel.DrinkDetailViewModel

class DrinkDetailViewModelFactory(private val detail:String): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        DrinkDetailViewModel(detail) as T
}