package com.example.bottomsup.view.drink

import com.example.bottomsup.model.response.CategoryDrinksDTO

data class DrinkListState(
     val isLoading: Boolean = false,
     val drinks: List<CategoryDrinksDTO.Drink> = emptyList()
 )