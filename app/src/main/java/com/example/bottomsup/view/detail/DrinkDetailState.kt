package com.example.bottomsup.view.detail

import com.example.bottomsup.model.response.DrinkDetailsDTO

data class DrinkDetailState (
    val isLoading: Boolean = false,
    val details: List<DrinkDetailsDTO.Drink> = emptyList()
        )