package com.example.bottomsup.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.bottomsup.adapter.CategoryAdapter
import com.example.bottomsup.databinding.FragmentCategoryBinding
import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.viewmodel.CategoryViewModel

class CategoryFragment : Fragment() {

    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!
    private val categoryViewModel by viewModels<CategoryViewModel>()

    private val categoryAdapter by lazy { CategoryAdapter(::categoryClicked) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvCategory.adapter = categoryAdapter
        categoryViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.loader.isVisible = state.isLoading
            categoryAdapter.loadCategories(state.categories)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun categoryClicked(category: CategoryDTO.CategoryItem) {
        Toast.makeText(context, category.toString(), Toast.LENGTH_LONG).show()
        val drinkListDirection =
            CategoryFragmentDirections.actionCategoryFragmentToDisplayFragment(category.strCategory)
        findNavController().navigate(drinkListDirection)
    }
}