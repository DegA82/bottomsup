package com.example.bottomsup.view.drink

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bottomsup.viewmodel.DrinkListViewModel

class DrinkListViewModelFactory(private val category: String) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T = DrinkListViewModel(category) as T

}