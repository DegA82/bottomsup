package com.example.bottomsup.view.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.bottomsup.adapter.DetailAdapter
import com.example.bottomsup.databinding.FragmentDrinkDetailBinding
import com.example.bottomsup.viewmodel.DrinkDetailViewModel

class DrinkDetailFragment : Fragment() {

    private var _binding: FragmentDrinkDetailBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DrinkDetailFragmentArgs>()
    private val drinkDetailViewModel by viewModels<DrinkDetailViewModel>() {
        DrinkDetailViewModelFactory(args.drinkId)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinkDetailViewModel.state.observe(viewLifecycleOwner) { state ->
            if (state.details.isNotEmpty()) {
                binding.rvDetail.adapter = DetailAdapter(state.details) { detail ->
                    Toast.makeText(context, detail.strInstructions, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}