package com.example.bottomsup.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.example.bottomsup.R
import com.example.bottomsup.viewmodel.CategoryViewModel

class MainActivity : AppCompatActivity() {

    private  val categoryViewModel by viewModels<CategoryViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        categoryViewModel
    }
}