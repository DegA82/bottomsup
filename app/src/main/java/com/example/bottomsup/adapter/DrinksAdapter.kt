package com.example.bottomsup.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.bottomsup.databinding.ItemDisplayBinding
import com.example.bottomsup.model.response.CategoryDrinksDTO
import com.example.bottomsup.view.drink.DisplayFragmentDirections

class DrinksAdapter(
    private val drinks: List<CategoryDrinksDTO.Drink>,
    private val drinkClicked: (CategoryDrinksDTO.Drink) -> Unit
) : RecyclerView.Adapter<DrinksAdapter.DisplayViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = DisplayViewHolder.getInstance(parent).apply {
        itemView.setOnClickListener {
            drinkClicked(drinks[adapterPosition])
        }
    }

    override fun onBindViewHolder(holder: DisplayViewHolder, position: Int) {
        val detail = drinks[position]
        holder.bindDisplay(detail)
    }

    override fun getItemCount(): Int {
        return drinks.size
    }

    class DisplayViewHolder(
        private val binding: ItemDisplayBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bindDisplay(drink: CategoryDrinksDTO.Drink) = with(binding) {
//            binding.idDrink.text=category.strDrinkThumb
            tvDrink.text = drink.strDrink

            //using glide
            Glide.with(root.context).load(drink.strDrinkThumb).into(ivDrink)

            mcDisplay.setOnClickListener {
                it.findNavController().navigate(
                    DisplayFragmentDirections.actionDisplayFragmentToDrinkDetailFragment(drink.idDrink)
                )
            }
            //using picasso
//            Picasso.get().load(drink.strDrinkThumb).into(ivDrink)

            //Coil
//            ivDrink.load(drink.strDrinkThumb)
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDisplayBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> DisplayViewHolder(binding) }
        }
    }
}