package com.example.bottomsup.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.bottomsup.databinding.ItemDetailBinding
import com.example.bottomsup.model.response.DrinkDetailsDTO

class DetailAdapter(
    private val details: List<DrinkDetailsDTO.Drink>,
    private val detailClicked: (DrinkDetailsDTO.Drink) -> Unit
) : RecyclerView.Adapter<DetailAdapter.DetailViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DetailViewHolder.getInstance(parent).apply {
            itemView.setOnClickListener { detailClicked(details[adapterPosition]) }
        }

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        val detail = details[position]
        holder.bindingDetail(detail)
    }

    override fun getItemCount(): Int {
        return details.size
    }

    class DetailViewHolder(
        private val binding: ItemDetailBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindingDetail(detail: DrinkDetailsDTO.Drink) = with(binding) {
            tvDetail.text = detail.strDrink
            tvDetail.text = detail.strInstructions

            Glide.with(root.context).load(detail.strDrinkThumb).into(ivDetail)
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDetailBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> DetailViewHolder(binding) }
        }
    }
}