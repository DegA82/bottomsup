package com.example.bottomsup.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bottomsup.databinding.ItemCategoryBinding
import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.view.category.CategoryFragmentDirections

class CategoryAdapter(
    private val categoryClicked: (category: CategoryDTO.CategoryItem) -> Unit
) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private val categories = mutableListOf<CategoryDTO.CategoryItem>()

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = CategoryViewHolder.getInstance(parent).apply {
        itemView.setOnClickListener { categoryClicked(categories[adapterPosition]) }
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categories[position]
        holder.bindCategory(category)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    fun loadCategories(categories: List<CategoryDTO.CategoryItem>) {
        this.categories.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll(categories)
            notifyItemRangeInserted(0, categories.size)
        }
    }

    class CategoryViewHolder(
        private val binding: ItemCategoryBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindCategory(category: CategoryDTO.CategoryItem) = with(binding) {
            tvCategory.text = category.strCategory
            tvCategory.setOnClickListener {
                it.findNavController().navigate(
//                    CategoryFragmentDirections.actionCategoryFragmentToDisplayFragment(category.strCategory)
                    CategoryFragmentDirections.actionCategoryFragmentToDisplayFragment(category.strCategory)
                )
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CategoryViewHolder(it) }
        }
    }
}