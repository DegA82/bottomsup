package com.example.bottomsup.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.view.detail.DrinkDetailState
import kotlinx.coroutines.launch

class DrinkDetailViewModel(drinkId: String) : ViewModel() {
    private val repo = BottomsUpRepo

    private val _state = MutableLiveData(DrinkDetailState(isLoading = true))
    val state: LiveData<DrinkDetailState> get() = _state

    init {
        viewModelScope.launch {
            val drinkDetailsDTO = repo.getDrinksDetails(drinkId)
            _state.value = DrinkDetailState(details = drinkDetailsDTO.drinks)
        }
    }
}