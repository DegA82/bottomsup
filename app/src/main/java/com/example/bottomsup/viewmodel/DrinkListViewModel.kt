package com.example.bottomsup.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.view.drink.DrinkListState
import kotlinx.coroutines.launch

class DrinkListViewModel(category: String) : ViewModel() {

    private val repo = BottomsUpRepo

//    private val state: LiveData<DrinkListState> = liveData{
//        emit(DrinkListState(isLoading = true))
//        viewModelScope.launch {
//            val categoryDrink = repo   .getDrinksInCategory(category)
//            emit(DrinkListState(drinks = categoryDrink.drinks))
//        }
//    }

    private val _state = MutableLiveData(DrinkListState(isLoading = true))
    val state: LiveData<DrinkListState> get() = _state

    init {
        viewModelScope.launch {
            val categoryDrinks = repo.getDrinksInCategory(category)
            _state.value = DrinkListState(drinks = categoryDrinks.drinks)
        }
    }
}