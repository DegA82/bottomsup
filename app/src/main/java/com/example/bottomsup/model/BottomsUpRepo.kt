package com.example.bottomsup.model

import com.example.bottomsup.model.remote.BottomsUpService
import com.example.bottomsup.model.response.DrinkDetailsDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object BottomsUpRepo {

    //instance of service
    private val bottomsUpService by lazy { BottomsUpService.getInstance() }

    suspend fun getCategories() = withContext(Dispatchers.IO) {
        bottomsUpService.getCategories()
    }

    suspend fun getDrinksInCategory(category: String) = withContext(Dispatchers.IO) {
        bottomsUpService.getDrinksInCategory(category)
    }

    suspend fun getDrinksDetails(drinkId: String): DrinkDetailsDTO = withContext(Dispatchers.IO){
        bottomsUpService.getDrinksDetails(drinkId)
    }
}